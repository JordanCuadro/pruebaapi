<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{

    public function index(){
        return response()->json('Hola get',200);
    }


    public function store(Request $request){
        return response()->json('Hola post');
    }
}
